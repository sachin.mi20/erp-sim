const express = require('express');

const app = express();

app.get('/', (req, res) => {
    console.log(req.query.batchId.batchId);
    const batchId = req.query.batchId.batchId;
   if(batchId==="HPT002-22X-BEG2021-B1"){
        res.json({
            "students": [
                {
                    "admissionNumber": "22XBEG00019",
                    "name": "Aswin",
    
                },
                {
                    "admissionNumber": "22XBEG00020",
                    "name": "Gandhi",
    
                },
    
            ]
        })
    } 
    else if(batchId==="HPT002-22X-BEG2021-B2"){
        res.json({
            "students": [
                {
                    "admissionNumber": "22XBEG00022",
                    "name": "Aswini",
    
                },
                {
                    "admissionNumber": "22XBEG00023",
                    "name": "Gajani",
    
                },
    
            ]
        })
    }
    else if(batchId==="HPT002-22X-BEG2021-B3"){
        res.json({
            "students": [
                {
                    "admissionNumber": "22XBEG00024",
                    "name": "Bose",
    
                },
                {
                    "admissionNumber": "22XBEG00025",
                    "name": "Singh",
    
                },
    
            ]
        })
    }
    })
    

app.listen(3000, () => {
        console.log('Server is running on port 3000');
    })
    
    